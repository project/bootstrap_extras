<?php

namespace Drupal\bootstrap_extras;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Helper class that holds all the main Barrio helper functions.
 */
class Barrio {

  /**
   * Utility function to return CSS classes.
   */
  public static function getClasses($name = 'region') {
    static $classes = [];

    if (!isset($classes[$name])) {
      $classes[$name] = [];
      // Prevent the name from being changed.
      $name_clone = $name;
      \Drupal::moduleHandler()->alter('barrio_classes', $classes[$name], $name_clone);
    }

    return $classes[$name];
  }

}
